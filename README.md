# Panfletos Nano [XNO]

O objetivo deste projeto é ajudar a Nano a alcançar as pessoas que realmente precisam dela.

A ideia é adaptar os ótimos panfletos feitos para a criptomoeda Monero, só que para a Nano, de forma a sofisticar a competição dessas moedas no livre mercado.

## Doações

Aceito doações no seguinte endereço: nano_3qdgq4r59196t587izitfghcibikag4z181e6qntqwbfwzksbwd68n9qgo84

## O que é Nano?

Nano é uma criptomoeda que utiliza a arquitetura Block Lattice para solucionar várias das limitações da arquitetura Blockchain pura e simples.

## O que é Block Lattice?

A principal diferença é: ao invés de possuir uma única Blockchain para todas as pessoas, ela possui uma (ou mais) Blockchain por pessoa, e as entrelaça por meio da arquitetura Block Lattice.

Ou seja, ao invés de ser um enorme livro-razão único para o mundo inteiro, é uma estrutura que permite que cada pessoa tenha um ou mais livros-razão, e os compartilhe na rede, para que todos os nós tenham cópias de todos os livros-razão, mas de forma que permita o processamento assíncrono de transações.

## Onde aprender sobre Nano?

Site oficial: [Nano | Moeda digital ecológica e sem taxas](https://nano.org/pt)

Lives:

- [Live com Spark Sobre Nano e Bitcoin - YouTube](https://www.youtube.com/watch?v=9KxrFMPBH00)

- [NANO É LUZ!!! - Papo Critpo sobre Nano com Spark! Proteja seu patrimônio e resista ao Estado! - YouTube](https://www.youtube.com/watch?v=vLZWTPz3HKA)

Grupos de discussão:

- [Nano Brasil - Telegram](https://t.me/NanoBrasil)

- https://comunidadenanobrasil.com/

## Observações

O diretório ```inkscape-svg``` contém as artes no formato SVG para serem usadas no programa Inkscape.

O diretório `plain-svg` contém as artes no formato SVG para serem usadas em qualquer programa.

## Licença

A licença é a Creative Commons Zero (CC0) , e está localizada nos diretórios das artes sobre as quais a licença se aplica.
